package ejercicios.capitulo.pkg2;
import java.util.Scanner;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Par_o_Impar {
    public void main(){
        System.out.println("<<Programa que identifica si un número es par o impar>>");
        // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        int x; // primer número introducido por el usuario
        System.out.print("Escriba un numero entero: "); // indicador de entrada
        x = entrada.nextInt(); // lee el tercer entero
        if((x%2)== 0){
            System.out.printf("El número %d es par %n",x );
        }
        if((x%2)!= 0){
            System.out.printf("El número %d es impar %n",x );
        }
    }
}
