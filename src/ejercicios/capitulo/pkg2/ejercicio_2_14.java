/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios.capitulo.pkg2;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class ejercicio_2_14 {
    public void main(){
        System.out.println("<<Programa que escribre 1,2,3,4 en linea con diferentes print's>>");
        System.out.println("1 2 3 4");
        System.out.print("1 ");
        System.out.print("2 ");
        System.out.print("3 ");
        System.out.print("4\n");
        System.out.printf("%d %d %d %d%n",1,2,3,4);
    }
}
