package ejercicios.capitulo.pkg2;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Entero_caracter {
    public void main(){
        System.out.println("<<Programa que muestra el valor entero de ciertos caracteres>>");
        System.out.printf("El carácter %c tiene el valor %d%n", 'A', ((int) 'A'));
        System.out.printf("El carácter %c tiene el valor %d%n", 'B', ((int) 'B'));
        System.out.printf("El carácter %c tiene el valor %d%n", 'C', ((int) 'C'));
        System.out.printf("El carácter %c tiene el valor %d%n", 'a', ((int) 'a'));
        System.out.printf("El carácter %c tiene el valor %d%n", 'b', ((int) 'b'));
        System.out.printf("El carácter %c tiene el valor %d%n", 'c', ((int) 'c'));
        System.out.printf("El carácter %c tiene el valor %d%n", '0', ((int) '0'));
        System.out.printf("El carácter %c tiene el valor %d%n", '1', ((int) '1'));
        System.out.printf("El carácter %c tiene el valor %d%n", '2', ((int) '2'));
        System.out.printf("El carácter %c tiene el valor %d%n", '$', ((int) '$'));
        System.out.printf("El carácter %c tiene el valor %d%n", '*', ((int) '*'));
        System.out.printf("El carácter %c tiene el valor %d%n", '+', ((int) '+'));
        System.out.printf("El carácter %c tiene el valor %d%n", '/', ((int) '/'));
        System.out.printf("El carácter %c tiene el valor %d%n", ' ', ((int) ' '));
    }
    
}
