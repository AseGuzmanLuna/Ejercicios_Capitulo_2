package ejercicios.capitulo.pkg2;
import java.util.Scanner;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Comparación {
    public void main(){
        System.out.println("<<Programa que compara dos numeros>>");
        int x, y;//valores que introduce el usuario
         // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        System.out.print("Escriba el numero primer entero: "); // indicador de entrada
        x = entrada.nextInt(); // lee el primer entero
        System.out.print("Escriba el segundo numero entero: "); // indicador de entrada
        y = entrada.nextInt(); // lee el segundo entero
        if(x>y){
            System.out.printf("%d es mayor que %d%n",x,y);
        }
        if(y>x){
            System.out.printf("%d es mayor que %d%n",y,x);
        }
        if(x==y){
            System.out.println("Los numeros son iguales");
        }
    }
}
