package ejercicios.capitulo.pkg2;

import java.util.Scanner;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class ValoresNPCero {
    public void main(){
        System.out.println("<<Programa que determina de 5 números cuantos son negativos, cuantos son positivos y cuantos son cero>>");
        // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        int x, y ,z, w, v,n=0,p=0,c=0;
        System.out.print("Escriba el numero primer entero: "); // indicador de entrada
        v = entrada.nextInt(); // lee el primer entero
        if(v==0)
            c++;
        if(v<0)
            n++;
        if(v>0)
            p++;
        System.out.print("Escriba el segundo numero entero: "); // indicador de entrada
        w = entrada.nextInt(); // lee el segundo entero
        if(w==0)
            c++;
        if(w<0)
            n++;
        if(w>0)
            p++;
        System.out.print("Escriba el tercer numero entero: "); // indicador de entrada
        x = entrada.nextInt(); // lee el tercer entero
        if(x==0)
            c++;
        if(x<0)
            n++;
        if(x>0)
            p++;
        System.out.print("Escriba el cuarto numero entero: "); // indicador de entrada
        y = entrada.nextInt(); // lee el cuarto entero
        if(y==0)
            c++;
        if(y<0)
            n++;
        if(y>0)
            p++;
        System.out.print("Escriba el quinto numero entero: "); // indicador de entrada
        z = entrada.nextInt(); // lee el quinto entero
        if(z==0)
            c++;
        if(z<0)
            n++;
        if(z>0)
            p++;
        System.out.printf("Números iguales a 0: %d%n", c);
        System.out.printf("Números menores a 0: %d%n", n);
        System.out.printf("Números mayores a 0: %d%n", p);
    }
}
