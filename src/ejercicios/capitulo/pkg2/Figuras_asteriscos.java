/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios.capitulo.pkg2;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Figuras_asteriscos {
    public void main(){
        System.out.println("<<Programa que muestra un cuadro, un ovalo, una flecha y un diamante usando asteriscos (*)>>");
        System.out.println("*********   ***     *       *");
        System.out.println("*       *  *   *   ***     * *");
        System.out.println("*       * *     * *****   *   *");
        System.out.println("*       * *     *   *    *     *");
        System.out.println("*       * *     *   *   *       *");
        System.out.println("*       * *     *   *    *     *");
        System.out.println("*       * *     *   *     *   *");
        System.out.println("*       *  *   *    *      * *");
        System.out.println("*********   ***     *       *");
        
    }
    
}
