package ejercicios.capitulo.pkg2;

import java.util.Scanner;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Masa_Corporal {
    public void main(){
        System.out.println("<<Programa que calcula el índice de masa corporal>>");
        float peso, altura, BMI=0;
        int i;
        Scanner entrada = new Scanner(System.in);
        System.out.print("Si introducirá su peso en libras presione 1, si es en kg presione 0: ");
        i = entrada.nextInt();
        if(i==1){
            System.out.print("Introduzca su peso en libras: ");
            peso = entrada.nextFloat();
            System.out.print("Introduzca su altura en pulgadas: ");
            altura = entrada.nextFloat();
           BMI = (peso*703)/(altura*altura);
        }
        if(i==0){
            System.out.print("Introduzca su peso en kilogamos: ");
            peso = entrada.nextFloat();
            System.out.print("Introduzca su altura en metros: ");
            altura = entrada.nextFloat();
            BMI = peso/(altura*altura);
        }
        System.out.printf("Tu índice de masa corporal es: %f%n",BMI);
        System.out.println("Analiza en qué rango te encuentras:\nBajo peso: menos de 18.5\nNormal: entre 18.5 y 24.9\nSobrepeso: entre 25 y 29.9\nObeso: 30 o más");
    }
}
