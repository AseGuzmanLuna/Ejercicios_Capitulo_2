package ejercicios.capitulo.pkg2;

import java.util.Scanner;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Separacion_enteros {
    public void main(){
        System.out.println("<<Programa que muestra un numero entero de 5 dígitos dígito por dígito>>");
        // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        int x; // primer número introducido por el usuario
        int x1,x2,x3,x4,x5;
        System.out.print("Escriba un numero entero de 5 dígitos: "); // indicador de entrada
        x = entrada.nextInt(); // lee el tercer entero
        x1 = x/10000;
        x2 = (x%10000)/1000;
        x3 = (x%1000)/100;
        x4 = (x%100)/10;
        x5 = (x%10);
        System.out.printf("%d %d %d %d %d%n",x1, x2,x3, x4, x5);
    }
}
