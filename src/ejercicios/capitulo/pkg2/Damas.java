package ejercicios.capitulo.pkg2;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Damas {
    public void main(){
        System.out.println("<<Programa que muestra un tablero de damas con asteríscos (*)>>");
        System.out.println("* * * * * * * *");
        System.out.println(" * * * * * * * *");
        System.out.println("* * * * * * * *");
        System.out.println(" * * * * * * * *");
        System.out.println("* * * * * * * *");
        System.out.println(" * * * * * * * *");
        System.out.println("* * * * * * * *");
        System.out.println(" * * * * * * * *");
    }
}
