package ejercicios.capitulo.pkg2;

import java.util.Scanner;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Multiplo {
    public void main(){
        System.out.println("<<Programa que identifica si un número es múltiplo de otro>>");
        // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        int x; // primer número introducido por el usuario
        int y; // segundo número introducido por el usuario
        System.out.print("Escriba el numero primer entero: "); // indicador de entrada
        x = entrada.nextInt(); // lee el primer entero
        System.out.print("Escriba el segundo numero entero: "); // indicador de entrada
        y = entrada.nextInt(); // lee el segundo entero
        if((x%y)==0){
            System.out.printf("%d es múltiplo de %d%n",x, y);
        }
        if((x%y)!=0){
            System.out.printf("%d no es múltiplo de %d%n",x, y);
        }
    }
    
}
