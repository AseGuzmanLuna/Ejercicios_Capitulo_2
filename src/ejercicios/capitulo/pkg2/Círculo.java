package ejercicios.capitulo.pkg2;

import java.util.Scanner;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Círculo {
    public void main(){
        System.out.println("<<Programa que calcula área, diametro y circunferencia de un círcula dado su radio>>");
        // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        int x; // primer número introducido por el usuario
        System.out.print("Ingrese el valor del radio de un círculo: ");
        x = entrada.nextInt();
        System.out.printf("El diámetro del círculo es %d%n", x*2);//calcula el diametro e imprime
        System.out.printf("El área del círculo es %f%n", Math.PI*x*x);//calcuna el area e imrpime
        System.out.printf("La circunferencia del círculo es %f%n", Math.PI*x*2);//calcula la circunferencia e imprime
    }     
}
