package ejercicios.capitulo.pkg2;
import java.util.Scanner;
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Aritmética_menor_mayor {
    public void main(){
        System.out.println("<<Programa que da el menor y mayor de tres numeros dados>>");
        // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        int x; // primer número introducido por el usuario
        int y; // segundo número introducido por el usuario
        int z; // tercer número introducido por el usuario
        int resultado; // producto de los números
        System.out.print("Escriba el numero primer entero: "); // indicador de entrada
        x = entrada.nextInt(); // lee el primer entero
        System.out.print("Escriba el segundo numero entero: "); // indicador de entrada
        y = entrada.nextInt(); // lee el segundo entero
        System.out.print("Escriba el tercer numero entero: "); // indicador de entrada
        z = entrada.nextInt(); // lee el tercer entero
        System.out.printf("La suma es %d%n",x+y+z);
         System.out.printf("El promedio es %d%n",(x+y+z)/3);
        System.out.printf("El producto es %d%n",x*y*z);
        if(x>y&&x>z){
             System.out.printf("El número mayor es %d%n", x);
             if(y>z)
                 System.out.printf("El número menor es %d%n", z);
                 else{
                 System.out.printf("El número menor es %d%n", y);
                }        
        }
        if(y>x&&y>z){
             System.out.printf("El número mayor es %d%n", y);
             if(x>z)
                 System.out.printf("El número menor es %d%n", z);
                 else{
                 System.out.printf("El número menor es %d%n", x);
                }
        }
        if(z>x&&z>y){
             System.out.printf("El número mayor es %d%n", z);
             if(x>y)
                 System.out.printf("El número menor es %d%n", y);
                 else{
                 System.out.printf("El número menor es %d%n", x);
                }
        }
    }
}
