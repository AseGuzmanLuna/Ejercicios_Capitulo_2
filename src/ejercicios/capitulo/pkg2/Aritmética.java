package ejercicios.capitulo.pkg2;
import java.util.Scanner; // el programa usa Scanner
/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Aritmética {
    public void main(){
        System.out.println("<<Programa que suma, multiplica y divide dos numeros enteros>>");
        int x, y, suma, prod; //valores que introduce el usuario
        float div;
         // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        System.out.print("Escriba el numero primer entero: "); // indicador de entrada
        x = entrada.nextInt(); // lee el primer entero
        System.out.print("Escriba el segundo numero entero: "); // indicador de entrada
        y = entrada.nextInt(); // lee el segundo entero
        suma = x+y;//realiza la suma de los dos numeros
        prod = x*y;//realiza la multiplicacion de los dos numeros
        div = (float)x/y;//realiza la division de los dos numeros
        System.out.printf("La suma de los dos números es %d%n",suma);//imprime el valor de suma
        System.out.printf("El producto de los dos números es %d%n",prod);//imprime el valor de prod
        System.out.printf("La división de los dos números es %f%n",div);//imprime el valor de div
    }
    
}
