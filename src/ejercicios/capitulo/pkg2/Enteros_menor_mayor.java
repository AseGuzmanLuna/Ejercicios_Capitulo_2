package ejercicios.capitulo.pkg2;

import java.util.Scanner;

/**
 *
 * @author ASERET GUZMAN LUNA
 */
public class Enteros_menor_mayor {
    public void main(){
        System.out.println("<<Programa que compara 5 números>>");
        // crea objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        int x, y ,z, w, v,temp;
        System.out.print("Escriba el numero primer entero: "); // indicador de entrada
        v = entrada.nextInt(); // lee el primer entero
        System.out.print("Escriba el segundo numero entero: "); // indicador de entrada
        w = entrada.nextInt(); // lee el segundo entero
        if(w>v){
            temp=v;
            v=w;
            w=temp;
        }
        System.out.print("Escriba el tercer numero entero: "); // indicador de entrada
        x = entrada.nextInt(); // lee el tercer entero
        if(x>w){
            temp=w;
            w=x;
            x=temp;
                if(w>v){
                    temp=v;
                    v=w;
                    w=temp;
            }
        }
        System.out.print("Escriba el cuarto numero entero: "); // indicador de entrada
        y = entrada.nextInt(); // lee el cuarto entero
        if(y>x){
            temp=x;
            x=y;
            y=temp;
                if(x>w){
                    temp=w;
                    w=x;
                    x=temp;
                        if(w>v){
                            temp=v;
                            v=w;
                            w=temp;
                        }
                }
        }        
        System.out.print("Escriba el quinto numero entero: "); // indicador de entrada
        z = entrada.nextInt(); // lee el quinto entero
        if(z>y){
            temp=y;
            y=z;
            z=temp;
                if(y>x){
                    temp=x;
                    x=y;
                    y=temp;
                        if(x>w){
                            temp=w;
                            w=x;
                            x=temp;
                                if(w>v){
                                    temp=v;
                                    v=w;
                                    w=temp;
                                }
                        }
                }
        }
        System.out.printf("El numero menor es %d%n",z);
        System.out.printf("El numero mayor es %d%n",v);
    }
}
